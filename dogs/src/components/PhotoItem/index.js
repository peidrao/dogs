import { useEffect } from "react";
import { Link } from "react-router-dom";
import PhotoComments from "../PhotoComments";

import styles from "./style.module.css";

const PhotoItem = ({ photo }) => {
  console.log(photo);
  console.log("AIAIA");
  return (
    <div className={styles.photo}>
      <div className={styles.img}>
        <img
          src={`http://localhost:8080/static/${photo.image}`}
          alt={photo.name}
        />{" "}
      </div>
      <div className={styles.details}>
        <div>
          <p>
            <Link to={`/perfil/${photo.user_id}`}>@{photo.user_id}</Link>
            <span className={styles.views}> {photo.name} </span>
          </p>
          <h1 className="title">
            <Link to={`/foto/${photo.id}`}>{photo.name} </Link>
          </h1>
          <ul className={styles.attributes}>
            <li>{photo.weigth}</li>
            <li>{photo.age}</li>
          </ul>
        </div>
      </div>
      <PhotoComments id={photo.id} comments={[]} />
    </div>
  );
};

export default PhotoItem;
