import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { UserContext } from "../../context/userContext";
import useForm from "../../hooks/useForm";
import AuthenticationService from "../../services/authentication";
import UserService from "../../services/user";
import Button from "../Button";
import ButtonStyle from "../Button/style.module.css";
import Error from "../Error";
import Input from "../Input";

import styles from "./style.module.css";

const LoginForm = () => {
  const email = useForm();
  const password = useForm();

  const { userLogin, error, loading } = useContext(UserContext);

  async function handleSubmit(event) {
    event.preventDefault();
    if (email.validate() && password.validate()) {
      userLogin(email.value, password.value);
    }
  }

  return (
    <section className="animeLeft">
      <h1 className="title">Login</h1>
      <form className={styles.form} onSubmit={handleSubmit}>
        <Input label="E-mail" type="email" name="email" {...email} />
        <Input label="Senha" type="password" name="password" {...password} />
        {loading ? <Button>Carregando...</Button> : <Button>Entrar</Button>}
        <Error error={error} />
      </form>
      <Link className={styles.perdeu} to="/login/lost">
        Perdeu a Senha?
      </Link>

      <div className={styles.cadastro}>
        <h2 className={styles.subtitle}>Cadastre-se</h2>
        <p>Ainda nao possui conta? Cadastre-se</p>
        <Link className={ButtonStyle.button} to="/login/create">
          Cadastro
        </Link>
      </div>
    </section>
  );
};

export default LoginForm;
