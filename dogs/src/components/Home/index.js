import React from "react";
import Feed from "../Feed";
import styles from "./style.module.css";

const Home = () => {
  return (
    <section className="container mainContainer">
      <Feed />
    </section>
  );
};

export default Home;
