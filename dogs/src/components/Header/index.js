import React, { useContext } from "react";
import { Link } from "react-router-dom";

import styles from "./style.module.css";
import { ReactComponent as Dogs } from "../../assets/dogs.svg";
import { UserContext } from "../../context/userContext";

const Header = () => {
  const { data, userLogout } = useContext(UserContext);

  console.log(data);
  return (
    <header className={styles.header}>
      <nav className={`${styles.nav} container`}>
        <Link className={styles.logo} to="/" aria-label="Dogs - Home">
          <Dogs />
        </Link>
        {data ? (
          <Link className={styles.login} to="/account">
            {data.name}
          </Link>
        ) : (
          <Link className={styles.login} to="/login">
            Login
          </Link>
        )}
      </nav>
    </header>
  );
};

export default Header;
