import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { UserContext } from "../../context/userContext";
import useForm from "../../hooks/useForm";
import UserService from "../../services/user";
import Button from "../Button";
import Error from "../Error";
import Input from "../Input";

import styles from "./style.module.css";

const SignupForm = () => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const username = useForm();
  const email = useForm("email");
  const password = useForm();
  const name = useForm();

  const { userLogin } = useContext(UserContext);

  async function handleSubmit(event) {
    event.preventDefault();
    const service = new UserService();

    await service
      .createUser(name.value, username.value, email.value, password.value)
      .then((resp) => {
        setLoading(true);
        userLogin(email.value, password.value);
      })
      .catch((err) => {
        console.log(err);
        setError(err);
      })
      .finally(() => setLoading(false));
  }

  return (
    <section className="animeLeft">
      <h1 className="title">Cadastre-se</h1>
      <form onSubmit={handleSubmit}>
        <Input label="Nome" type="text" name="name" {...name} />
        <Input
          label="Nome de Usuario"
          type="text"
          name="username"
          {...username}
        />
        <Input label="E-mail" type="text" name="email" {...email} />
        <Input label="Senha" type="password" name="password" {...password} />
        {loading ? (
          <Button disabled>Cadastrando...</Button>
        ) : (
          <Button>Cadastrar</Button>
        )}
        <Error error={error} />
      </form>
    </section>
  );
};

export default SignupForm;
