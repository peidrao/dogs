import styles from "./style.module.css";

const FeedPhotoItem = ({ photo, setModalPhoto }) => {
  function handleClick() {
    setModalPhoto(photo);
  }

  return (
    <li className={styles.photo} onClick={handleClick}>
      <img
        src={`http://localhost:8080/static/${photo.image}`}
        alt={photo.title}
      />
      <span className={styles.view}>{photo.ID}</span>
    </li>
  );
};

export default FeedPhotoItem;
