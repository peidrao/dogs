import { useEffect } from "react";
import PhotoItem from "../PhotoItem";

import styles from "./style.module.css";

const FeedModal = ({ photo }) => {
  // useEffect(() => {}, [photo]);
  // console.log(photo);
  return (
    <div className={styles.modal}>{photo && <PhotoItem photo={photo} />}</div>
  );
};

export default FeedModal;
