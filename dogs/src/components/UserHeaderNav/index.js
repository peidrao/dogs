import { ReactComponent as FeedSvg } from "../../assets/feed.svg";
import { ReactComponent as StatsSvg } from "../../assets/estatisticas.svg";
import { ReactComponent as AddPhotoSvg } from "../../assets/adicionar.svg";
import { ReactComponent as LogoutSvg } from "../../assets/sair.svg";
import { NavLink, useLocation } from "react-router-dom";
import { useContext, useEffect, useState } from "react";

import { UserContext } from "../../context/userContext";
import useMedia from "../../hooks/useMedia";
import styles from "./style.module.css";

const UserHeaderNav = () => {
  const { userLogout } = useContext(UserContext);
  const mobile = useMedia("(max-width: 40rem)");
  const [mobileMenu, setMobileMenu] = useState(false);

  const { pathname } = useLocation();
  useEffect(() => {
    setMobileMenu(false);
  }, [pathname]);

  return (
    <>
      {mobile && (
        <button
          aria-label="Menu"
          className={`${styles.mobileButton} ${
            mobileMenu && styles.mobileButtonActive
          }`}
          onClick={() => setMobileMenu(!mobileMenu)}
        ></button>
      )}

      <nav
        className={`${mobile ? styles.navMobile : styles.nav} ${
          mobileMenu && styles.navMobileActive
        }`}
      >
        <NavLink to="/user" end>
          <FeedSvg />
          {mobile && "Minhas Fotos"}
        </NavLink>
        <NavLink to="/user/estatisticas">
          <StatsSvg />
          {mobile && "Estatisticas"}
        </NavLink>
        <NavLink to="/user/postar">
          <AddPhotoSvg />
          {mobile && "Adicionar Foto"}
        </NavLink>
        <button onClick={userLogout}>
          <LogoutSvg />
          {mobile && "Sair"}
        </button>
      </nav>
    </>
  );
};

export default UserHeaderNav;
