import React, { useContext, useState } from "react";
import Input from "../Input";
import Button from "../Button";
import Error from "../Error";
import useForm from "../../hooks/useForm";

import styles from "./style.module.css";
import PostService from "../../services/post";
import { UserContext } from "../../context/userContext";
import { useNavigate } from "react-router-dom";

const UserPhotoPost = () => {
  const name = useForm();
  const age = useForm("number");
  const weigth = useForm("number");
  const [image, setImage] = useState({});
  const [loading, setLoading] = useState(null);
  const { data } = useContext(UserContext);
  const navigate = useNavigate();
  const [error, setEror] = useState(null);

  async function handleSubmit(event) {
    event.preventDefault();
    setLoading(true);
    const formData = new FormData();
    const service = new PostService();

    formData.append("image", image.raw);
    formData.append("name", name.value);
    formData.append("weigth", weigth.value);
    formData.append("age", age.value);
    formData.append("user_id", data["id"]);

    service
      .createPost(formData)
      .then((response) => {
        if (response.status === 200) {
          navigate("/user");
        }
      })
      .catch((err) => setEror(err))
      .finally(setLoading(false));
  }

  function handleImageChange({ target }) {
    setImage({
      preview: URL.createObjectURL(target.files[0]),
      raw: target.files[0],
    });
  }

  return (
    <section className={`${styles.photoPost} animeLeft`}>
      <form onSubmit={handleSubmit}>
        <Input label="Nome" type="text" name="name" {...name} />
        <Input label="Peso" type="text" name="weigth" {...weigth} />
        <Input label="Idade" type="text" name="age" {...age} />
        <input
          className={styles.file}
          type="file"
          name="image"
          id="image"
          onChange={handleImageChange}
        />
        {loading ? (
          <Button disabled>Enviando...</Button>
        ) : (
          <Button>Enviar</Button>
        )}
        <Error error={error} />
      </form>
      <div>
        {image.preview && (
          <div
            className={styles.preview}
            style={{ backgroundImage: `url('${image.preview}')` }}
          ></div>
        )}
      </div>
    </section>
  );
};

export default UserPhotoPost;
