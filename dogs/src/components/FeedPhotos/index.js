import { useEffect, useState } from "react";
import FeedPhotoItem from "../FeedPhotoItem";
import Error from "../Error";
import Loading from "../Loading";
import PostService from "../../services/post";

import styles from "./style.module.css";

const FeedPhotos = ({ setModalPhoto }) => {
  const [photos, setPhotos] = useState([]);
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    async function fetchPhotos() {
      const service = new PostService();
      const response = await service.getPosts();
      setPhotos(response.data.data);
    }
    setLoading(true);
    fetchPhotos();
    setLoading(false);
  }, []);

  if (error) return <Error error={error} />;
  if (loading) return <Loading />;
  return (
    <div>
      <ul className={styles.feed}>
        {photos.map((photo) => (
          <FeedPhotoItem
            key={photo.ID}
            photo={photo}
            setModalPhoto={setModalPhoto}
          />
        ))}
      </ul>
    </div>
  );
};

export default FeedPhotos;
