import React, { useContext } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import PasswordRecovery from "../../components/PasswordRecovery";
import PasswordReset from "../../components/PasswordReset";
import SignupForm from "../../components/SignupForm";
import LoginForm from "../../components/LoginForm";

import styles from "./style.module.css";
import { UserContext } from "../../context/userContext";

const Login = () => {
  const { login } = useContext(UserContext);

  if (login === true) return <Navigate to="/user" />;

  return (
    <section className={styles.login}>
      <div className={styles.forms}>
        <Routes>
          <Route path="/" element={<LoginForm />} />
          <Route path="/create" element={<SignupForm />} />
          <Route path="/reset" element={<PasswordReset />} />
          <Route path="/recovery" element={<PasswordRecovery />} />
        </Routes>
      </div>
    </section>
  );
};

export default Login;
