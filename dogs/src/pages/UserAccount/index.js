import { Route, Routes } from "react-router-dom";
import UserHeader from "../../components/UserHeader";
import UserPhotoPost from "../../components/UserPhotoPost";
import UserStats from "../../components/UserStats";
import Feed from "../../components/Feed";

const UserAccount = () => {
  return (
    <section className="container">
      <UserHeader />
      <Routes>
        <Route path="/" element={<Feed />} />
        <Route path="/postar" element={<UserPhotoPost />} />
        <Route path="/estatisticas" element={<UserStats />} />
      </Routes>
    </section>
  );
};

export default UserAccount;
