import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Home from "./components/Home";
import Login from "./pages/Login";

import "./App.css";
import { UserStorage } from "./context/userContext";
import UserAccount from "./pages/UserAccount";
import ProtectedRoute from "./middlewares/protected";

function App() {
  return (
    <div>
      <BrowserRouter>
        <UserStorage>
          <Header />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login/*" element={<Login />} />
            <Route
              path="/user/*"
              element={
                <ProtectedRoute>
                  {" "}
                  <UserAccount />{" "}
                </ProtectedRoute>
              }
            />
          </Routes>
          <Footer />
        </UserStorage>
      </BrowserRouter>
    </div>
  );
}

export default App;
