import api from "../utils/api";

export default class PostService {
  async createPost(formData) {
    const headers = {
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    };

    const response = await api.post("/auth/posts", formData, headers);
    return { data: response.data, status: response.status };
  }

  async getPosts() {
    const headers = {
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    };

    const response = await api.get("/auth/posts", headers);
    return { data: response.data, status: response.status };
  }

  async getPost(postId) {
    const headers = {
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    };

    const response = await api.get(`/auth/posts/${postId}`, headers);
    return { data: response.data, status: response.status };
  }
}
