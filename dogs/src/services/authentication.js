import api from "../utils/api";

export default class AuthenticationService {
  async login(email, password) {
    const response = await api.post("/token", { email, password });

    return {
      token: response.data.token,
      status: response.status,
    };
  }

  async tokenIsValid(token) {
    const headers = {
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    };
    const response = await api.post("/auth/token_is_valid", { token }, headers);
    return { status: response.status };
  }
}
