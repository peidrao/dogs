import api from "../utils/api";

export default class UserService {
  async me() {
    const headers = {
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("token")}`,
      },
    };
    const response = await api.get("/auth/me", headers);
    return { data: response.data, status: response.status };
  }

  async createUser(name, username, email, password) {
    const response = await api.post("/user/register", {
      name: name,
      username: username,
      email: email,
      password: password,
    });
    return { data: response.data, status: response.status };
  }
}
