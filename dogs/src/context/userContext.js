import { createContext, useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import AuthenticationService from "../services/authentication";
import UserService from "../services/user";

export const UserContext = createContext();

export const UserStorage = ({ children }) => {
  const [data, setData] = useState(null);
  const [login, setLogin] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const userLogout = useCallback(
    async function () {
      setData(null);
      setError(null);
      setLoading(false);
      setLogin(false);
      await window.localStorage.removeItem("token");
      navigate("/login");
    },
    [navigate]
  );

  async function getUser() {
    const service = new UserService();
    const response = await service.me();
    setLogin(true);
    setData(response.data);
  }

  async function userLogin(email, password) {
    try {
      setError(null);
      setLoading(true);
      const service = new AuthenticationService();
      const { token } = await service.login(email, password);
      window.localStorage.setItem("token", token);

      await getUser();
      navigate("/user");
    } catch (err) {
      setError(err.response.data.error);
      setLogin(false);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    async function autoLogin() {
      const token = window.localStorage.getItem("token");
      if (token) {
        try {
          setError(null);
          setLoading(true);
          const service = new AuthenticationService();
          const { status } = await service.tokenIsValid(token);
          if (status !== 200) {
            throw new Error("Invalid token");
          }
          await getUser();
        } catch (err) {
          userLogout();
        } finally {
          setLoading(false);
        }
      }
    }
    autoLogin();
  }, [userLogout]);

  return (
    <UserContext.Provider
      value={{ userLogin, data, userLogout, error, loading, getUser, login }}
    >
      {children}
    </UserContext.Provider>
  );
};
