package controllers

import (
	"api-auth/database"
	"api-auth/models"
	"api-auth/repo"
	"api-auth/utils"
	"mime/multipart"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type FormPost struct {
	Image  *multipart.FileHeader `form:"image"`
	Name   string                `form:"name"`
	Weigth float32               `form:"weigth"`
	Age    uint64                `form:"age"`
	UserId uint64                `form:"user_id"`
}

func CreatePost(context *gin.Context) {
	var form FormPost
	var post models.Post
	if err := context.ShouldBind(&form); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	file, err := context.FormFile("image")

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "No file is received"})
		context.Abort()
		return
	}

	extension := filepath.Ext(file.Filename)
	newFileName := uuid.New().String() + extension

	if err := context.SaveUploadedFile(file, "./static/"+newFileName); err != nil {
		context.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Unable to save the file"})
		return
	}

	post = models.Post{Image: newFileName, Name: form.Name, Weigth: float64(form.Weigth), Age: uint(form.Age), UserId: uint(form.UserId)}
	record := database.Instance.Create(&post)

	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"message": "Your file has been successfully uploaded."})
}

func GetPosts(context *gin.Context) {
	var post models.Post
	pagination := utils.GeneratePaginationFromRequest(context)

	posts, totalRows, err := repo.GetAllPosts(&post, &pagination)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": posts, "total": totalRows})
}

func GetPost(context *gin.Context) {
	var post *models.Post
	ID, err := strconv.ParseUint(context.Param("post_id"), 10, 64)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	post, err = repo.GetPostByID(post, ID)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err})
		return

	}

	context.JSON(http.StatusOK, post)
}

func DeletePost(context *gin.Context) {
	var post models.Post
	ID := context.Param("post_id")

	database.Instance.Where("id = ?", ID).Delete(&post)
	context.AbortWithStatus(http.StatusNoContent)
}
