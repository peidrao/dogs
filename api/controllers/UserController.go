package controllers

import (
	"api-auth/database"
	"api-auth/middleware"
	"api-auth/models"
	"net/http"

	"github.com/gin-gonic/gin"
)

type UserStruct struct {
	Email string `json:"email"`
	Name  string `json:"name"`
}

type ProfileMeStruct struct {
	ID       uint   `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Name     string `json:"name"`
}

func RegisterUser(context *gin.Context) {
	var user models.User
	if err := context.ShouldBindJSON(&user); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	if err := user.HashPassword(user.Password); err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	record := database.Instance.Create(&user)

	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}

	context.JSON(http.StatusCreated, gin.H{"userID": user.ID, "email": user.Email, "username": user.Username})
}

func ListAllUsers(context *gin.Context) {
	var users []models.User

	database.Instance.Find(&users)

	context.JSON(http.StatusOK, users)
}

func ProfileMe(context *gin.Context) {
	var profile models.User
	user_id, err := middleware.ExtractTokenID(context)
	if err != nil {

		context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	database.Instance.First(&profile, user_id)

	response := ProfileMeStruct{ID: profile.ID, Email: profile.Email, Username: profile.Username, Name: profile.Name}
	context.JSON(http.StatusOK, response)
}
