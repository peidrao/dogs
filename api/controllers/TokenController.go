package controllers

import (
	"api-auth/database"
	"api-auth/middleware"
	"api-auth/models"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type TokenRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func LoginController(context *gin.Context) {
	var request TokenRequest
	var user models.User

	if err := context.ShouldBindJSON(&request); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	record := database.Instance.Where("email = ?", request.Email).First(&user)
	if record.Error != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": record.Error.Error()})
		context.Abort()
		return
	}

	credentialError := user.CheckPassword(request.Password)
	if credentialError != nil {
		context.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid credentials"})
		context.Abort()
		return
	}

	tokenString, err := generateToken(uint64(user.ID))
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		context.Abort()
		return
	}

	context.JSON(http.StatusOK, gin.H{"token": tokenString})
}

func generateToken(userID uint64) (string, error) {
	claims := jwt.MapClaims{}
	claims["authorized"] = true

	claims["user_id"] = userID
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(1)).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString([]byte("yoursecretstring"))
}

func TokenIsValid(context *gin.Context) {
	err := middleware.TokenValid(context)
	// jwtToken, err := middleware.ExtractBearerToken(context.GetHeader("Authorization"))
	// if err != nil {
	// 	context.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
	// 	return
	// }

	// err = auth.ValidateToken(jwtToken)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	context.JSON(http.StatusOK, gin.H{"message": "ok"})
}
