package repo

import (
	"api-auth/database"
	"api-auth/models"
	"api-auth/utils"
)

func GetAllPosts(post *models.Post, pagination *utils.Pagination) (*[]models.Post, int64, error) {
	var posts []models.Post
	var totalRows int64 = 0

	offset := (pagination.Page - 1) * pagination.Limit
	queryBuider := database.Instance.Limit(pagination.Limit).Offset(offset).Order(pagination.Sort)
	result := queryBuider.Model(&models.Post{}).Find(&posts)
	if result.Error != nil {
		msg := result.Error
		return nil, totalRows, msg
	}

	errCount := database.Instance.Model(&models.Post{}).Count(&totalRows).Error
	if errCount != nil {
		return nil, totalRows, errCount
	}

	return &posts, totalRows, nil
}

func GetPostByID(post *models.Post, postID uint64) (*models.Post, error) {
	// var comments []models.Comment
	result := database.Instance.Model(&post).Preload("Comments").Find(&post, postID)
	// result := database.Instance.Preload("Post").Where("id = ?", postID).Find(&comments)
	if result.Error != nil {
		return nil, result.Error
	}
	return post, nil
}
