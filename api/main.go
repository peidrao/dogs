package main

import (
	"api-auth/controllers"
	"api-auth/database"
	"api-auth/middleware"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	// Initialize database
	database.Connect("user:password@tcp(localhost:3306)/db?parseTime=true")
	database.Migrate()
	// Initialize Routes
	router := initRouter()

	router.Run(":8080")
}

func initRouter() *gin.Engine {
	router := gin.Default()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowCredentials = true
	config.AddAllowHeaders("authorization")
	router.Use(cors.New(config))
	router.Static("/static", "./static")
	api := router.Group("/api")

	api.POST("/token", controllers.LoginController)
	api.POST("/user/register", controllers.RegisterUser)

	protected := router.Group("/api/auth")
	protected.Use(middleware.JwtAuthMiddleware())
	protected.GET("/me", controllers.ProfileMe)
	protected.POST("/token_is_valid", controllers.TokenIsValid)
	protected.GET("/users", controllers.ListAllUsers)
	protected.POST("/posts", controllers.CreatePost)
	protected.GET("/posts", controllers.GetPosts)
	protected.GET("/posts/:post_id", controllers.GetPost)
	protected.DELETE("/posts/:post_id", controllers.DeletePost)
	protected.GET("/", controllers.Hello)

	return router
}
