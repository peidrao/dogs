package models

import "gorm.io/gorm"

type Post struct {
	gorm.Model
	Image    string    `json:"image"`
	Name     string    `json:"name"`
	Weigth   float64   `json:"weigth"`
	Age      uint      `json:"age"`
	UserId   uint      `json:"user_id"`
	Comments []Comment `json:"comments"`
}

type Comment struct {
	gorm.Model
	ID      uint64 `json:"id"`
	Message string `json:"message"`
	PostID  string `json:"post_id"`
	UserId  string `json:"user_id"`
}
